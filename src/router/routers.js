import LoginContainer from "../view/containers/login";
import Home from "../view/pages/home";
import NotFound from "../view/pages/404";
const routers = [
  {
    path: "/login",
    component: LoginContainer,
    exact: true
  },
  {
    path: ["/", "/home"],
    component: Home,
    exact: true
  },
  {
    component: NotFound,
    exact: true
  }
];
export default routers;
