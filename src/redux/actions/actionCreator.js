import { LOGIN_SUCCESS, LOGIN_FAILD } from "./actions";
export const loginSuccess = (access_token, refresh_token, authority) => ({
  type: LOGIN_SUCCESS,
  payload: { access_token, refresh_token, authority }
});

export const loginFailed = () => ({
  type: LOGIN_FAILD
});
