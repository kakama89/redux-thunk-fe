import axios from "axios";
export const loginAction = (username, password) => {
  return dispatch => {
    // const url = "http://localhost:8080/eship/test?sleep=1";
    const url = "http://localhost:8080/eship/api/users/login";
    const payload = { username, password };
    axios
      .post(url, payload, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(resp => {
        let { data } = resp;
        console.log("login success........ fire loginActionStart :", data);
        localStorage.setItem("auth", JSON.stringify({ ...data }));
        // dispatch(
        //   loginSuccess(data.access_token, data.refresh_token, data.authority)
        // );
      })
      .catch(error => {
        const { data } = error.response;
        console.log(data);
        alert(data.code);
        localStorage.clear("auth");
        // console.log("login failed........ fire loginActionError ");
        // dispatch(loginFailed());
      });
  };
};
