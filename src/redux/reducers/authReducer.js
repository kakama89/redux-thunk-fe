import { LOGIN_SUCCESS, LOGIN_FAILD } from "../actions/actions";

const authReducer = (state = { auth: { isAuthenticated: false } }, action) => {
  console.log("payload", action.payload);
  switch (action.type) {
    case LOGIN_SUCCESS:
      return Object.assign({}, state, {
        auth: {
          isAuthenticated: true,
          ...action.payload
        }
      });
    case LOGIN_FAILD:
      return Object.assign({}, state, {
        auth: {
          isAuthenticated: false
        }
      });
    default:
      return state;
  }
};
export default authReducer;
