import React, { Component } from "react";
class Login extends Component {
  state = { username: "", password: "" };

  changeUsername = e => {
    const target = e.target || e.which;
    this.setState({ username: target.value });
  };

  changePassword = e => {
    const target = e.target || e.which;
    this.setState({ password: target.value });
  };

  render() {
    return (
      <React.Fragment>
        <div className="row pt-2 pb-2">
          <div className="col col-md-1">Email</div>
          <div className="col col-md-3">
            <input
              type="text"
              className="pl-2 w-100"
              placeholder="Email"
              onChange={this.changeUsername}
              value={this.state.username}
            />
          </div>
        </div>
        <div className="row pt-2 pb-2">
          <div className="col col-md-1">Password</div>
          <div className="col col-md-3">
            <input
              type="password"
              className="pl-2 w-100"
              placeholder="Password"
              onChange={this.changePassword}
              value={this.state.password}
            />
          </div>
        </div>
        <div className="row pt-2 pb-2">
          <div className="col">
            <button
              type="submit"
              onClick={() =>
                this.props.handleLogin(this.state.username, this.state.password)
              }
              className="w-20"
            >
              Login
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Login;
