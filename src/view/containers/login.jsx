import { connect } from "react-redux";
import Login from "../pages/login";
import { loginAction } from "../../redux/actions/thunkAction";
const mapDispathToProps = dispatch => {
  return {
    handleLogin: (username, password) => {
      dispatch(loginAction(username, password));
    }
  };
};
export default connect(null, mapDispathToProps)(Login);
