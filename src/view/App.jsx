import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import routers from "../router/routers";
import Header from "./components/header";
import Footer from "./components/footer";
import LoginContainer from './containers/login';
import _auth from '../auth/auth';

const App = () => {
  const auth = _auth();
  return (
    <React.Fragment>
      <Header />
      <div className="container">
        
        { auth && auth.access_token ? <Router>
          <Switch>
            {routers.map(router => {
              return <Route key={router.path} {...router} />;
            })}
          </Switch>
        </Router> : <LoginContainer/> }
      </div>
      <Footer />
    </React.Fragment>
  );
};
export default App;
